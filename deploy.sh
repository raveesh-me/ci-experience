#!/bin/sh
export user=ubuntu;
export machine=15.207.19.154;
ssh -o StrictHostKeyChecking=no $user@$machine -i dummytobedeleted.pem /bin/bash <<EOF
echo "Entered Script at $(date)" >> log.txt
git clone https://gitlab.com/raveesh-me/ci-experience.git;
cd ci-experience;
git checkout master;
git pull;
sudo docker rm -f $(docker ps -aq);
sudo docker run -v "$(pwd)/"deploy:/usr/share/nginx/html:ro -p 8080:80 -d nginx >> log.txt;
EOF
